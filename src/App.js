import React, { Component } from "react";
import "./App.css";
import { Table, Input, Button, Space } from "antd";
import { Row, Col } from "antd";
import axios from "axios";
import "antd/dist/antd.css";
import { Form, InputNumber } from "antd";

const { Search } = Input;

const api = axios.create({
  baseURL: `http://localhost:8762`,
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      concerts: [],
      sessions: [],
      tickets: [],
      notification: ""
    };
    api.get("/api/concert/all").then((res) => {
      this.setState({ concerts: res.data });
      console.log(this.state.concerts);
    });
    api.get("/session/session/all").then((res) => {
      this.setState({ sessions: res.data });
      console.log(this.state.sessions);
    });
    api.get("/ticket/ticket/all").then((res) => {
      this.setState({ tickets: res.data });
      console.log(this.state.tickets);
    });
  }

  handleSubmitConcert = (e) => {
    e.id = Number(e.id);
    e.location = String(e.location);
    e.description = String(e.description);
    e.name = String(e.name);
    axios
      .post("http://localhost:8762/api/concert/admin/create", e)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  handleSubmitSesssion = (e) => {
    e.id = Number(e.id);
    e.time = String(e.time);
    api.get(`/api/concert/id/${e.concert}`).then((res) => {
      // console.log(res.data);
      e.concert = res.data;
      axios
      .post("http://localhost:8762/session/session/admin/create", e)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    });
  };

  handleSubmitTicket = (e) => {
    e.id = Number(e.id);
    e.price = Number(e.price);
    e.quantity = Number(e.quantity);
    api.get(`/session/session/id/${e.session}`).then((res) => {
      api.get(`/ticket/place/id/1`).then((resPlace) => {
        e.session = res.data;
        e.place = resPlace.data;
        axios
        .post("http://localhost:8762/ticket/ticket/admin/create", e)
        .then((res) => console.log(res))
        .catch((err) => console.log(err));
      })
      // console.log(res.data);
    });
  };

  handleSubmitBooking = (e) => {
    e.quantity = Number(e.quantity);
    api.get(`/client/user/username/${e.username}`).then((res) => {
      api.get(`/ticket/ticket/id/${e.ticket}`).then((resTicket) => {
        e.client = res.data;
        e.ticket = resTicket.data;
        axios
        .post("http://localhost:8762/booking/booking/create", e)
        .then((resBooking) => console.log(resBooking))
        .catch((err) => console.log(err));
      })
      api.get(`http://localhost:8762/booking/booking/request?clientId=1&bookingId=1`).then((res) => {
              this.setState({ notification: res.data });
              console.log(this.state.notification);
            })
      // console.log(res.data);
    });
  };
  
  getByConcertName = (e) => {
    api.get(`/api/concert/name/${e}`).then((res) => {
      console.log(res.data);
      this.setState({ searchConcerts: res.data })
    });
  };

  render() {
    const layout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };

    const columnsConcerts = [
      {
        title: "ID",
        dataIndex: "id",
      },
      {
        title: "Concert Name",
        dataIndex: "name",
      },
      {
        title: "Location",
        dataIndex: "location",
      },
      {
        title: "Description",
        dataIndex: "description",
      },
    ];

    const columnsSession = [
      {
        title: "ID",
        dataIndex: "id",
      },
      {
        title: "Session Time",
        dataIndex: "time",
      },
    ];

    const columnsTicket = [
      {
        title: "ID",
        dataIndex: "id",
      },
      {
        title: "Price",
        dataIndex: "price",
      },
      {
        title: "Quantity",
        dataIndex: "quantity",
      },
    ];

    const textStyle = {
      textAlignVertical: "center",
      textAlign: "center",
    };

    return (
      <div className="App">
        <h1>Ticketon Ticket System</h1>
        <hr></hr>
        <Row>
          <Col>
            <h2>Add Concert</h2>
            <Form
              {...layout}
              name="nest-messages"
              onFinish={this.handleSubmitConcert}
            >
              <Form.Item label="ID" name={"id"}>
                <Input />
              </Form.Item>
              <Form.Item label="Name" name={"name"}>
                <Input />
              </Form.Item>
              <Form.Item label="Description" name={"description"}>
                <Input />
              </Form.Item>
              <Form.Item label="Location" name={"location"}>
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">
                  Create
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col>
            <h2>Add Session</h2>
            <Form
              {...layout}
              name="nest-messages"
              onFinish={this.handleSubmitSesssion}
            >
              <Form.Item label="ID" name={"id"}>
                <Input />
              </Form.Item>
              <Form.Item label="Time" name={"time"}>
                <Input />
              </Form.Item>
              <Form.Item label="Concert ID" name={"concert"}>
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">
                  Create
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col>
            <h2>Add Ticket</h2>
            <Form
              {...layout}
              name="nest-messages"
              onFinish={this.handleSubmitTicket}
            >
              <Form.Item label="ID" name={"id"}>
                <Input />
              </Form.Item>
              <Form.Item label="Session" name={"session"}>
                <Input />
              </Form.Item>
              <Form.Item label="Price" name={"price"}>
                <Input />
              </Form.Item>
              <Form.Item label="Quantity" name={"quantity"}>
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">
                  Create
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col>
            <h2>Booking</h2>
            <Form
              {...layout}
              name="nest-messages"
              onFinish={this.handleSubmitBooking}
            >
              <Form.Item label="ID" name={"id"}>
                <Input />
              </Form.Item>
              <Form.Item label="Ticket ID" name={"ticket"}>
                <Input />
              </Form.Item>
              <Form.Item label="Client" name={"username"}>
                <Input />
              </Form.Item>
              <Form.Item label="Quantity" name={"quantity"}>
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                <Button type="primary" htmlType="submit">
                  Book
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <hr></hr>
        <Row>
          <Col style={{ marginLeft: 30 }}>
            <Table
              columns={columnsConcerts}
              dataSource={this.state.concerts}
              bordered
              title={() => "concerts"}
            />
          </Col>
          <Col style={{ marginLeft: 30 }}>
            <Table
              columns={columnsSession}
              dataSource={this.state.sessions}
              bordered
              title={() => "sessions"}
            />
          </Col>
          <Col style={{ marginLeft: 30 }}>
            <Table
              columns={columnsTicket}
              dataSource={this.state.tickets}
              bordered
              title={() => "tickets"}
            />
          </Col>
          <h2>{this.state.notification}</h2>
        </Row>
      </div>
    );
  }
}

export default App;
